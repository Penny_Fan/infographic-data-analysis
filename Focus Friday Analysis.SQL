-- Focus Time
SELECT
    week_of_year,
    day_of_week,
    local_date,
	CASE WHEN account_id in (565463,571631) THEN 'Customer Facing'
		ELSE 'Non-Customer Facing' END AS employee_type,
    ROUND(SUM(focus_duration_seconds) / COUNT(DISTINCT CONCAT(CONCAT(account_id,user_id),local_date)) / 3600,2) AS focus_hr,
	SUM(focus_duration_seconds) / SUM(total_duration_seconds) AS focus_efficiency
FROM `productivity-lab-291116.Labs.focus_friday_analysis_q4_vs_q1`
WHERE day_type = 'Weekday' AND local_date NOT IN ('2020-2-17','2020-5-25','2020-7-3','2020-9-7','2020-11-26','2020-11-27','2020-12-24','2020-12-25','2021-1-1','2021-2-15')
GROUP BY 1,2,3,4
ORDER BY 3 ASC;


-- Focus
SELECT
    DATE(activity_datetime_utc_start,'America/Chicago') AS local_date,
	CASE WHEN EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) = 2 THEN 'Monday'
		WHEN EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) = 3 THEN 'Tuesday'
		WHEN EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) = 4 THEN 'Wednesday'
		WHEN EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) = 5 THEN 'Thursday'
		WHEN EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) = 6 THEN 'Friday'
		WHEN EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) = 7 THEN 'Saturday'
		WHEN EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) = 1 THEN 'Sunday'
	END AS day_of_week,
	CASE WHEN account_id in (565463,571631) THEN 'Customer Facing'
		ELSE 'Non-Customer Facing' END AS employee_type,
    SUM(duration_seconds)/ COUNT(row_id) / 60 AS AVG_Focus_Session_mins
FROM `productivity-lab-291116.Internal_Accounts.user_session_log_all`
WHERE session_type = 'Focus'
AND EXTRACT(DAYOFWEEK FROM DATE(activity_datetime_utc_start,'America/Chicago')) IN (2,3,4,5,6) AND DATE(activity_datetime_utc_start,'America/Chicago') NOT IN ('2020-2-17','2020-5-25','2020-7-3','2020-9-7','2020-11-26','2020-11-27','2020-12-24','2020-12-25','2021-1-1','2021-2-15','2021-2-16','2021-2-17','2021-2-18','2021-2-19')
GROUP BY 1,2,3
ORDER BY 1 DESC


-- Avg Slack Session Mins including: AVG_Slack_Frequency_min, AVG_Slack_Sessions_count, AVG_Slack_Session_min
WITH slack_log AS (
SELECT
    local_date,
    day_of_week,
    accountid,
    user_name,
    user_id,
    local_datetime,
    DATETIME_DIFF(
        LEAD(local_datetime) OVER (PARTITION BY accountid,
                                                    user_name,
                                                    user_id,
                                                    local_date,
                                                    day_of_week
                                        ORDER BY local_datetime ASC),
        DATETIME_ADD(local_datetime,  INTERVAL duration_sec SECOND),
        SECOND) AS seconds_till_next_activity
FROM `productivity-lab-291116.Internal_Accounts.events_all`
WHERE REGEXP_CONTAINS(application_or_site, r"((?i).*slack).*") AND local_date >= '2020-1-1' AND local_date <= '2021-3-31' AND day_type = 'Weekday' AND local_date NOT IN ('2020-2-17','2020-5-25','2020-7-3','2020-9-7','2020-11-26','2020-11-27','2020-12-24','2020-12-25','2021-1-1','2021-2-15')
)
SELECT
    local_date,
    day_of_week,
    AVG(seconds_till_next_activity)/60 as AVG_Slack_Session
FROM slack_log
GROUP BY 1,2
ORDER BY 1 DESC


-- Slack Sessions
WITH slack_log AS (
SELECT
    local_date,
    day_of_week,
    accountid,
    user_name,
    user_id,
    local_datetime,
    LEAD(local_datetime) OVER (PARTITION BY accountid,
                                                    user_name,
                                                    user_id,
                                                    local_date,
                                                    day_of_week
                                        ORDER BY local_datetime ASC) AS activity_datetime_next_activity,
    DATETIME_DIFF(
        LEAD(local_datetime) OVER (PARTITION BY accountid,
                                                    user_name,
                                                    user_id,
                                                    local_date,
                                                    day_of_week
                                        ORDER BY local_datetime ASC),
        DATETIME_ADD(local_datetime,  INTERVAL duration_sec SECOND),
        SECOND) AS seconds_till_next_activity
FROM `productivity-lab-291116.Internal_Accounts.events_all`
WHERE REGEXP_CONTAINS(application_or_site, r"((?i).*slack).*") AND local_date >= '2020-1-1' AND local_date <= '2021-3-31' AND day_type = 'Weekday' AND local_date NOT IN ('2020-2-17','2020-5-25','2020-7-3','2020-9-7','2020-11-26','2020-11-27','2020-12-24','2020-12-25','2021-1-1','2021-2-15','2021-2-16','2021-2-17','2021-2-18','2021-2-19')
AND duration_sec < 1800
)
,slack_usage AS(
    SELECT
    local_date,
    day_of_week,
    SUM(duration_sec)/60 AS Total_Slack_Time_min,
    SUM(duration_sec) / COUNT(DISTINCT CONCAT(CONCAT(accountid,user_id),local_date))/60 AS AVG_Slack_Time_min,
FROM `productivity-lab-291116.Internal_Accounts.events_all`
WHERE day_type = 'Weekday' AND local_date >= '2020-1-1' AND local_date <= '2021-3-31'
AND local_date NOT IN ('2020-2-17','2020-5-25','2020-7-3','2020-9-7','2020-11-26','2020-11-27','2020-12-24','2020-12-25','2021-1-1','2021-2-15','2021-2-16','2021-2-17','2021-2-18','2021-2-19')
AND (REGEXP_CONTAINS(application_or_site, r"((?i).*zoom).*") OR REGEXP_CONTAINS(application_or_site, r"((?i).*slack).*"))
AND duration_sec < 1800
GROUP BY 1,2
)

SELECT
    slack_session.local_date,
    slack_session.day_of_week,
    slack_session.AVG_Slack_Frequency_min,
    slack_session.AVG_Slack_Sessions_count,
    -- slack_usage.AVG_Slack_Time_min,
    slack_usage.Total_Slack_Time_min / slack_session.Total_Slack_Sessions_count  AS AVG_Slack_Session_min
FROM
(SELECT
    local_date,
    day_of_week,
    AVG(seconds_till_next_activity)/60 AS AVG_Slack_Frequency_min,
    COUNT(*) AS Total_Slack_Sessions_count,
    COUNT(*)/COUNT(DISTINCT CONCAT(accountid,user_id)) AS AVG_Slack_Sessions_count
FROM slack_log
WHERE seconds_till_next_activity != 0

GROUP BY 1,2
ORDER BY 1 DESC) slack_session
JOIN slack_usage ON slack_session.local_date = slack_usage.local_date
ORDER BY local_date DESC;
