-- Avg. Productive Hrs on 10/04/2021, avg prod session, avg focus session, news and media time, and fb search terms
-- Scale: 10/4/2021 vs the week before, Sept 2021, last 4 Mondays
select a.*,e.avg_facebook_activity_occurence,e.total_facebook_activity_occurence,b.social_media_mins,c.News_Entertainment_mins--,d.industry,d.sub_industry,d.company_employee_size
from
(SELECT 
    -- account_id,
    CASE WHEN local_date = '2021-10-4' THEN '10/04/2021'
        WHEN local_date BETWEEN '2021-9-27' AND '2021-10-1' THEN 'last_week'
        WHEN local_date IN ('2021-8-30','2021-9-13','2021-9-20','2021-9-27') THEN 'last 4 Mondays' 
        WHEN local_date between '2021-9-1' and '2021-9-30'and extract(dayofweek from local_date) not in (1,7) and local_date != '2021-9-6' then 'Sept_2021' end as time_window,
    CASE WHEN COUNT(1) != 0 THEN SUM(productive_active_duration_seconds + productive_passive_duration_seconds)/COUNT(1)/3600 END as avg_productive_hrs,
    CASE WHEN SUM(productive_session_count) != 0 THEN SUM(productive_session_duration_seconds)/SUM(productive_session_count)/60 END as avg_productive_session_mins,
    CASE WHEN SUM(focused_session_count) !=0 THEN SUM(focused_session_duration_seconds)/SUM(focused_session_count)/60 END as avg_focused_session_mins,
    CASE WHEN SUM(break_count) !=0 THEN SUM(break_duration_seconds)/SUM(break_count)/60 END as avg_break_mins,
    CASE WHEN COUNT(1) != 0 THEN SUM(non_business_activity_count)/COUNT(1) end as avg_non_business_activity_occurence
FROM `self-service-data.Daily_User_Summary_Enhanced.*`
group by 1
having time_window is not null) a
LEFT JOIN 
(select 
    -- account_id,
    CASE WHEN local_date = '2021-10-4' THEN '10/04/2021'
        
        WHEN local_date BETWEEN '2021-9-27' AND '2021-10-1' THEN 'last_week'
        WHEN local_date IN ('2021-8-30','2021-9-13','2021-9-20','2021-9-27') THEN 'last 4 Mondays'
        WHEN local_date between '2021-9-1' and '2021-9-30'and extract(dayofweek from local_date) not in (1,7) and local_date != '2021-9-6' then 'Sept_2021' end as time_window,
        CASE WHEN COUNT(DISTINCT CONCAT(account_id,CONCAT(local_date,user_name))) != 0 THEN COUNT(1)/COUNT(DISTINCT CONCAT(account_id,CONCAT(local_date,user_name))) end as avg_facebook_activity_occurence,
        COUNT(1) as total_facebook_activity_occurence
from `self-service-data.Daily_Application_Summary.*`
where application_or_site like '%facebook%'
group by 1
having time_window is not null) e on a.time_window = e.time_window --and a.account_id=e.account_id
left join 
(select 
    -- account_id,
    CASE WHEN local_date = '2021-10-4' THEN '10/04/2021'
    
        WHEN local_date BETWEEN '2021-9-27' AND '2021-10-1' THEN 'last_week'
        WHEN local_date IN ('2021-8-30','2021-9-13','2021-9-20','2021-9-27') THEN 'last 4 Mondays'
        WHEN local_date between '2021-9-1' and '2021-9-30'and extract(dayofweek from local_date) not in (1,7) and local_date != '2021-9-6' then 'Sept_2021' end as time_window,
        CASE WHEN COUNT(DISTINCT CONCAT(account_id,CONCAT(local_date,user_name))) != 0 THEN SUM(duration_seconds )/COUNT(DISTINCT CONCAT(account_id,CONCAT(local_date,user_name)))/60 end as social_media_mins
from `self-service-data.Daily_Application_Summary.*`
where category  = 'Social Media'
group by 1
having time_window is not null) b on a.time_window = b.time_window --and a.account_id=b.account_id
left join
(select 
    -- account_id,
    CASE WHEN local_date = '2021-10-4' THEN '10/04/2021'
    
        WHEN local_date BETWEEN '2021-9-27' AND '2021-10-1' THEN 'last_week'
        WHEN local_date IN ('2021-8-30','2021-9-13','2021-9-20','2021-9-27') THEN 'last 4 Mondays' 
        WHEN local_date between '2021-9-1' and '2021-9-30'and extract(dayofweek from local_date) not in (1,7) and local_date != '2021-9-6' then 'Sept_2021' end as time_window,
        CASE WHEN COUNT(DISTINCT CONCAT(account_id,CONCAT(local_date,user_name))) != 0 THEN SUM(duration_seconds )/COUNT(DISTINCT CONCAT(account_id,CONCAT(local_date,user_name)))/60 end as News_Entertainment_mins
from `self-service-data.Daily_Application_Summary.*`
where category  = 'News & Entertainment'
group by 1
having time_window is not null) c on b.time_window = c.time_window --and b.account_id=c.account_id


